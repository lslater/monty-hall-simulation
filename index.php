<?php

function run($switch) {
	$car = rand(0,2);
	$choice = rand(0,2);
	$reveal = false;
	for ($i=0; $i<3; $i++) {
		if ($car!=$i && $choice!=$i) {
			$reveal = $i;
			break;
		}
	}
	if ($switch) {
		for ($i=0; $i<3; $i++) {
			if ($reveal!=$i && $choice!=$i) {
				$choice = $i;
				break;
			}
		}
	}
	if ($choice==$car) {
		return true;
	} else {
		return false;
	}
}

$count_switched = 0;
$count_stayed = 0;

for ($i=0; $i<100; $i++) {
	if (run(true)) {
		$count_switched++;
	}
}

for ($i=0; $i<100; $i++) {
	if (run(false)) {
		$count_stayed++;
	}
}

echo "Number of wins when switching: $count_switched<br>";
echo "Number of wins when staying: $count_stayed<br>";
